# ACM Summer School Workshop 2019
# Cache and Memory Compression Techniques

## Introduction
The purpose of this lab section is to evaluate various compression algorithms on typical in-memory data.

### System Requirements
The tools in this repository run on any 64-bit Linux environment.
It may also be possible to build in other POSIX settings, such as Mac OSX or BSD.
Windows 10 includes the [Windows Subsystem for Linux](https://docs.microsoft.com/en-us/windows/wsl/install-win10) which may be helpful.

The files are also on the `Nord1` cluster made available to summer school participants:

```
/gpfs/scratch/nct01/nct01095/acm_workshop_2019
```

Refer to the "Practical Information" handout for your login credentials.

**If you are working on the cluster, please copy the source code to your own workspace but use the memory dumps where they already are, they are ~2GB of static data**


### Memory Dumps
A memory dump for a running program can be created with a Linux command as follows:
```
sudo gcore [-o <FILE-TO-DUMP>] PID
```

`PID`: The Process ID of the program

It is recommended to suspend the process before taking the dump to avoid stale memory data due to in-progress swap outs/ins. 

For more info, run `man gcore`

This lab package contains several such memory dumps from SPEC2017 benchmarks, and you should not need to create any new ones.


## The compr tool
Included in this package is the file `compr.cpp` which is able to manipulate the memory dumps described above.

It consists of some parsing code requiring the GNU BFD library, and has a hole where a compression algorithm should go.

To compile the tool, ensure you have `make`, `g++` and `libbfd` installed and invoke
```
make compr 
```

Invoke the tool on one of the memory dumps, for example:
```
./compr dumps/namd.core
```


As distributed, the tool applies no compression and thus outputs a compression ratio of `1.0` regardless of input.
Update the function `compress_line(cacheline* line)` to try out various compression techniques.

The code is written to iterate over individual 64-B cachelines, but the dataset is available as a giant, contiguous byte array in `compress(uint8_t* bytes, size_t size)` for you to try out other approaches as well.


## Additional algorithms
Two additional tools are provided, which illustrate state-of-the-art compression methods (BDI[1], FPC[2][3], ZCA[4], DuD[5], SC2[6], and HyComp[7].)
These are available with their documentation in `bdi_fpc_zca/` and `zeropoint/` respectively.
Both tools operate on the same memory dumps as your `compr`, and output the achievable compression ratio using their respective compression algorithms.


## Considerations
Be aware that compressing the data may also require some metadata, such as a compression dictionary or a bit indicating whether a block is compressed or not.
This metadata must also be included in the compressed size! Techniques such as Attache[8] attempt to avoid some of this overhead by clever encoding. Other techniques store some metadata in a separate table rather than mixed in with the data, and must thus deal with the latency and on-chip storage required to do so.

Different algorithms deal with different data types, and more than one datatype is represented in each memory dump. Consider trying hybrid approaches to
identify which of several methods work best for a given block!

The `compr` tool is written to count bytes. Feel free to modify the code to count bits instead, if this makes a difference to your method (and if you can justify how a hardware design could benefit from blocks compressed to partial bytes).

# References

[1] Gennady Pekhimenko, Vivek Seshadri, Onur Mutlu, Phillip B. Gibbons, Michael A. Kozuch, Todd C. Mowry. Base-delta-immediate compression: practical data compression for on-chip caches. PACT 2012.

[2] Alaa R. Alameldeen, David A. Wood. Adaptive Cache Compression for High-Performance Processors. ISCA 2004.

[3] Alaa R. Alameldeen and David A. Wood. Frequent Pattern Compression: A Significance-Based Compression Scheme for L2 Caches.

[4] Julien Dusser, Thomas Piquet, Andre Seznec. Zero-content augmented caches. ICS 2009.

[5] DuD is a simple dictionary alg. that Angelos Arelakis implemented during his PHD and it simply replaces common values within a block with pointers to dictionary entries, where this dictionary is dynamically formed and is kept part of the block. Similar algorithms have been implemented in the past by others too.

[6] Angelos Arelakis and Per Stenstrom. 2014. SC^2: A statistical compression cache scheme. ISCA 2014.

[7] Angelos Arelakis, Fredrik Dahlgren, and Per Stenstrom. 2015. HyComp: A hybrid cache compression method for selection of data-type-specific compression methods. MICRO-48.

[8] S. Hong et al. Attache: Towards Ideal Memory Compression by Mitigating Metadata Bandwidth Overheads. MICRO 2018
