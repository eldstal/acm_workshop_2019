## The BDI/FPC/ZCA/DuD compression tool
Author: Angelos Arelakis, 2019-06-27

This compression analysis tool can analyze cache or memory compression using the following algorithms:  BDI[1], FPC[2], ZCA[3] and DuD[4]. 

The tool is pre-configured to process on 64-B cache-lines, thus cache/memory binary files with mis-aligned file size are "pruned" (from the end of the file) by the tool.


### Build
The tool was built as follows on an X86-64 machine:
```
g++ -static -std=c++11 BDI_FPC_ZCA_DUD.cpp -o BDI_FPC_ZCA_DUD.o
```


### Run
Running the tool:
```
./BDI_FPC_ZCA_DUD.o <PATH-TO-FILENAME> <PATH-TO-RESULTS-FILENAME>
```
`PATH-TO-FILENAME`: A memory dump in binary form

`PATH-TO-RESULTS-FILENAME`: The file to dump the analysis status and results


### Memory Dump
A memory dump for a running program can be created with a Linux command as follows:
```
sudo gcore [-o <FILE-TO-DUMP>] PID
```

`PID`: The Process ID of the program

It is recommended to suspend the process before taking the dump to avoid stale memory data due to in-progress swap outs/ins. 

For more info, run `man gcore`

This lab package contains several such memory dumps from SPEC2017 benchmarks.


### Example Results File
```
>>>>>> STATUS
Total Size --> 18545536
After subtracting remainder : (NEW) Total Size --> 18545536
Non-Null blocks --> 280831  Null blocks --> 8943
>>>>>> END OF STATUS

Filename        BDI FPC DuD NullBlks  ZCA
/work/tmp/original.trc_PARSED.bin 1.02  1.02  1.04  8943  1.03
```



[1] Gennady Pekhimenko, Vivek Seshadri, Onur Mutlu, Phillip B. Gibbons, Michael A. Kozuch, Todd C. Mowry. Base-delta-immediate compression: practical data compression for on-chip caches. PACT 2012.

[2] Alaa R. Alameldeen, David A. Wood. Adaptive Cache Compression for High-Performance Processors. ISCA 2004.

[3] Julien Dusser, Thomas Piquet, Andre Seznec. Zero-content augmented caches. ICS 2009.

[4] DuD is a simple dictionary alg. that Angelos Arelakis implemented during his PHD and it simply replaces common values within a block with pointers to dictionary entries, where this dictionary is dynamically formed and is kept part of the block. Similar algorithms have been implemented in the past by others too. 


