#!/bin/bash

DIR=$(cd `dirname $0` && pwd)
PATH_TO_TOOL="${DIR}"
PATH_TO_FILENAME=$1
PATH_TO_RESULTS_FILENAME=$2

if [ -z "$PATH_TO_FILENAME" ] || [ -z "$PATH_TO_RESULTS_FILENAME" ] || [ -z "$PATH_TO_TOOL" ]; then
  echo "One of the required paths is missing :"
  echo "PATH_TO_TOOL --> $PATH_TO_TOOL"
  echo "PATH_TO_FILENAME --> \$1"
  echo "PATH_TO_RESULTS_FILENAME --> \$2"
else
  TMP=tmp
  mkdir -p $TMP
  ${PATH_TO_TOOL}/ZeroPoint --snapshot-path ${PATH_TO_FILENAME} --output-path $TMP --compressor SC2 --zpt set-assoc 4096 8 0 31 --sampler 1 10.0 64 --compressor NULL --zca 0 31 > ${PATH_TO_RESULTS_FILENAME}
  rm -r $TMP

  if [ -e "${PATH_TO_TOOL}/data_out.bin" ]; then rm ${PATH_TO_TOOL}/data_out.bin; fi
fi
