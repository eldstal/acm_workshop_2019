# DISCLAIMER:
This compression analysis tool is owned by ZeroPoint Technologies AB. 
It is shared for the ACM Europe Summer School 2019
and it is intended to be used for educational purposes only.

It is not allowed to be used for any commercial purpose.
For any other use, permission must be granted by  
ZeroPoint Technologies AB. 
info@zptcorp.com


# SC2 / HyComp tool
Author: Angelos Arelakis, ZeroPoint Technologies AB, 2019-07-14

This compression analysis tool can analyze cache or memory compression using ZeroPoint's proprietary algorithms (similar to SC2 [1]).
It can also combine this with other algorithms, e.g., Null-block compression, providing in the end the compression analysis for ZeroPoint's hybrid (similar to HyComp [2]).  
The tool is pre-configured to process 64-B cache-lines, thus cache/memory binary files with mis-aligned file size are "pruned" (from the end of the file) by the tool.


## Build
The tool was built as follows on an X86-64 machine:
```
g++ -static -std=c++11 -O2 ZeroPoint.cpp -o ZeroPoint
```


## Run
Running the tool: A simple bash script is provided to simplify running the tool.
Otherwise the tool command is:
```
${PATH_TO_TOOL}/ZeroPoint --snapshot-path ${PATH_TO_FILENAME} --output-path $TMP --compressor SC2 --zpt set-assoc 4096 8 0 31 --sampler 1 10.0 64 --compressor NULL --zca 0 31 > ${PATH_TO_RESULTS_FILENAME}
```

where:

`PATH_TO_TOOL`: Path where the ZeroPoint executable is 

`PATH_TO_FILENAME`: A memory dump in binary form

`PATH_TO_RESULTS_FILENAME`: The file to dump the analysis status and results.

`TMP`: Path where the tool generates some temporary files that are not needed. However, a valid path must be provided. 

ZeroPoint's algorithm --zpt requires some configuration for the training:

* VFT configuration: set-assoc, 4096 entries, 8-way set associative
* Training: --sampler 1 10.0 64: i.e., random (10% of the target snapshot) data analysis by scanning cache lines (64B)


## Memory Dumps
A memory dump for a running program can be created with a Linux command as follows:
```
sudo gcore [-o <FILE-TO-DUMP>] PID
```

`PID`: The Process ID of the program

It is recommended to suspend the process before taking the dump to avoid stale memory data due to in-progress swap outs/ins. 

For more info, run `man gcore`

In this lab package, several dumps are provided in `../dumps/`

## Example Results File
```
Training the selected Statistical compressor
Training: 10% completed

SC2
Compression: 100% completed
CR: 3.66879


NULL
Compression: 100% completed
CR: 1.09707

ZeroPoint's Hybrid Compression
Compression: 100% completed
CR: 3.70091
```


[1] Angelos Arelakis and Per Stenstrom. 2014. SC^2: A statistical compression cache scheme. ISCA 2014.
[2] Angelos Arelakis, Fredrik Dahlgren, and Per Stenstrom. 2015. HyComp: A hybrid cache compression method for selection of data-type-specific compression methods. MICRO-48. 

